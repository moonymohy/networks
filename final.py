from datetime import datetime
import socket
from struct import *
import pcapy
import sys

sniff_mode = True
raw_data = 0
def eth_addr( bin_addr):
    byte_str = map('{:02x}'.format, bin_addr)
    mac_addr = ':'.join(byte_str).upper()
    return mac_addr 


def sniffing(Dev):
    global raw_data
    conn = socket.socket(socket.SOCK_RAW, socket.SOCK_RAW, socket.ntohs(3))
    #packet = pcapy.open_live(Dev,65536,1,0)
    while(sniff_mode):
        raw_data, addr = conn.recvfrom(65535)
        #(header,packets)= packet.next()
        packetIdentifier(raw_data)
    return


def packetIdentifier(packet):
    #length of imp message
    eth_length = 14
    eth_header = packet[:eth_length]
    #unpacking
    eth = unpack('!6s 6s H' , eth_header)
    
    #definig main Info about pacakge
    eth_protocol = socket.ntohs(eth[2])
    Destination  = eth_addr(packet[0:6])
    Source = eth_addr(packet[6:12])
    Protocol =  str(eth_protocol)
    data = raw_data[14:]
    Arrive_time = datetime.now()
    Basic_Info = [Destination,Source,Protocol,Arrive_time]
    return Protocol , data , Basic_Info


def dataIdentifier(Protocol,data):
    if (Protocol == 8):
        #IPv4
        #ip addr len is 20
        ip_header = data[:20]
        #unpack
        iph = unpack('! 8x B B 2x 4s 4s' , ip_header)
        version_header_length = ip_header[0]
        #split them to version & header_len   
        Version = str(version_header_length >> 4)
        header_len = (version_header_length & 0xF) * 4
        IPHeaderLength = str(header_len)
        #rest of addr
        TTL = str(iph[5])
        proto = iph[6]
        IProtocol = str(proto)
        SourceAddress = str (socket.inet_ntoa(iph[8]))
        DestinationAddress = str(socket.inet_ntoa(iph[9])) 
        Idata = iph[header_len:]
    #if not ipv4 protocol
    else :
        print("Protocol not supported")
        return
    IPV4_Info = [Version, IPHeaderLength, TTL,IProtocol,SourceAddress, DestinationAddress]
    return proto, Idata , IPV4_Info


def TCP(data):
    tcp_header = data[:14]
    #unpack
    tcph = unpack('!H H L L B B H H H',tcp_header)
    #tcp main data  
    source_port = tcph[0]
    Source_Port =str(source_port)
    dest_port = tcph[1]
    Dest_Port = str(dest_port)
    sequence = tcph[2]
    Sequence_Number =str(sequence)
    acknowledgement = tcph[3]
    Acknowledgement = str(acknowledgement)
    offset_reserved_flags = tcph[4] 
    offset = (offset_reserved_flags >> 12) * 4
    TCP_header_length = str(offset)
    #flags
    flag_urg = (offset_reserved_flags & 32) >> 5
    URG = str (flag_urg)
    flag_ack = (offset_reserved_flags & 16) >> 4
    ACK = str (flag_ack)
    flag_psh = (offset_reserved_flags & 8) >> 3 
    PSH = str (flag_psh)
    flag_rst = (offset_reserved_flags & 4) >> 2  
    RST = str (flag_rst)
    flag_syn = (offset_reserved_flags & 2) >> 1 
    SYN = str (flag_syn)
    flag_fin = offset_reserved_flags & 1 
    FIN = str (flag_fin)
    IIdata = data[offset:] 
    if (source_port == 80 or dest_port == 80):
        ProtoType_Inf = [ 'HTTP', Source_Port , Dest_Port, Sequence_Number,Acknowledgement,TCP_header_length]
    else:
        ProtoType_Inf = [ 'TCP', Source_Port , Dest_Port, Sequence_Number,Acknowledgement,TCP_header_length]

    Flags = [URG,ACK,PSH,RST,SYN,FIN]
    ProtoType_Info = [list(a) for a in zip(ProtoType_Inf, Flags)]
    return IIdata,ProtoType_Info.ravel()

           
def ICPM(data):
    icpm_header = data[:4]
    #unpack
    icmph = unpack('!B B H',icpm_header)
    #icpm main data
    icmp_type = icmph[0]
    Type =  str(icmp_type)
    code = icmph[1]
    Code =  str(code)
    checksum = icmph[2]
    Checksum = str(checksum)
    IIdata = data[4:]
    Final_Data(IIdata)
    ProtoType_Info = ['ICPM' ,Type , Code , Checksum]   
    return IIdata ,ProtoType_Info


def UDP(data):
    udp_header = data[:8] 
    #unpack
    udph = unpack('!H H H H',udp_header)
    #udp main data
    source_port = udph[0]
    Source_Port = str(source_port)
    dest_port = udph[1]
    Dest_Port = str(dest_port)
    length = udph[2]
    Length = str(length)
    checksum = udph[3]
    Checksum = str(checksum)
    IIdata = data [8:]
    Final_Data(IIdata)
    ProtoType_Info = ['UDP' ,Source_Port , Dest_Port , Length,Checksum]          
    return IIdata , ProtoType_Info
            
def Final_Data(data):
    Data = str(data)
    Data_Length = len(Data)
    Final_Data = [Data,Data_Length]
    return Final_Data
    
def HexPacket(packet):
    hexa = []
    for i in range(1,len(packet)-8):
        temp = int(packet[i:i+8],2)
        hexa.append(hex(temp))
    return hexa


def run(device):
    sniffing(device)
    Protocol , data , Basic_Info = packetIdentifier(raw_data)
    proto , Idata , IPV4_Info = dataIdentifier(Protocol,data)
    #analyze the data type
    if (proto==6):
        IIdata , ProtoType_Info = TCP(Idata)
    elif(proto==1):
        IIdata , ProtoType_Info = ICPM(Idata)
    elif(proto==17):
        IIdata , ProtoType_Info = UDP(Idata)
    else :
        print("Protocol Not Supported")    
    Last_Data = Final_Data(IIdata)
    preview = HexPacket(raw_data)
    return Basic_Info , IPV4_Info , ProtoType_Info , Last_Data , preview
